package ru.god.task3;

import java.util.Scanner;

/**
 * Класс для перевода рублей в евро по заданному курсу
 *
 * @author Горбачева, 16ИТ18к
 */
public class Task3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введиту текущий курс\n1 евро = ");
        double course = scanner.nextDouble();
        System.out.println("Введие количество рублей");
        double rubles = scanner.nextDouble();
        System.out.printf("%.2f рублей = %.2f евро при текущем курсе 1 евро = %.2f рублей",
                rubles, euro(rubles, course), course);
    }

    /**
     * Метод для вычисления количества евро, которое можно купить
     * на определенное количество рублей по заданному курсу
     *
     * @param rubles - количество рублей
     * @param course - текущий курс обмена
     * @return количество евро
     */
    private static double euro(double rubles, double course) {
        return rubles / course;
    }
}
