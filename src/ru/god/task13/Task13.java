package ru.god.task13;

import java.util.Scanner;

/**
 * Класс, позволяющий определить, является ли символ буквой, цифрой или знаком пунктуации
 *
 * @author Горбачева, 16ИТ18к
 */
public class Task13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите символ, чтобы определить, является ли он буквой, цифрой или знаком пунктуации");
        char symbol = scanner.nextLine().charAt(0);
        System.out.println(info(symbol));
    }

    /**
     * Метод, передающий сообщение о введенном символе
     *
     * @param symbol - символ
     * @return сообщение о том, чем является символ (буквой, цифрой или знаком пунктуации)
     */
    private static String info(char symbol) {
        if (Character.isLetter(symbol)) {
            return ("Символ является буквой");
        }
        if (Character.isDigit(symbol)) {
            return ("Символ является цифрой");
        }
        return ("Символ является знаком пунктуации");
    }
}
