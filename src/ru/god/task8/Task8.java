package ru.god.task8;

import java.util.Scanner;

/**
 * Класс, позволяющий изменять значения определенного столбца двумерного массива
 *
 * @author Горбачева, 16ИТ18к
 */
public class Task8 {
    public static void main(String[] args) {
        int[][] matrix = {{2, 5, 6}, {5, 3, 9}, {4, 7, 10}};
        Scanner scanner = new Scanner(System.in);
        print(matrix);
        System.out.println("Введите номер столбца, который хотите обнулить");
        int number = scanner.nextInt();
        matrix = deleting(matrix, number - 1);
        print(matrix);
    }

    /**
     * Метод, позволяющий обнулить значения указанного столбца матрицы
     *
     * @param matrix         - исходная матрица
     * @param numberOfColumn - номер столбца
     * @return конечная матрица
     */
    private static int[][] deleting(int[][] matrix, int numberOfColumn) {
        for (int[] element : matrix) {
            element[numberOfColumn] = 0;
        }
        return matrix;
    }

    /**
     * Метод, позволяющий вывести на экран двумерный массив
     *
     * @param matrix - двумерный массив
     */
    private static void print(int[][] matrix) {
        for (int[] element : matrix) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(element[j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }
}
