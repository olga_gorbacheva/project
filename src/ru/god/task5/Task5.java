package ru.god.task5;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Класс, позволяющий получить все простые числа на указанном диапазоне
 *
 * @author Горбачева, 16ИТ18к
 */
public class Task5 {
    private static final String ENTER_INITIAL_VALUE = "Введите начальное значение диапазона";
    private static final String CHANGE_INITIAL_VALUE = "Числа меньше единицы не относятся к простым. Попробуйте ввести другое значение";
    private static final String ENTER_FINAL_VALUE = "Введите конечное значение диапазона";
    private static final String CHANGE_FINAL_VALUE = "Вы ввели некорректное значение. Попробуйте еще раз";
    private static final String RANGE_LIMITS = "На промежутке от %s до %s";
    private static final String NO_NUMBERS = " простых чисел не обнаружено";
    private static final String PRINT_NUMBERS = " простыми являются следующие числа:";
    /**
     * Массив чисел
     */
    private static ArrayList<Integer> arrayList = new ArrayList<>();
    /**
     * Начальное значение диапазона
     */
    private static int initialValue;
    /**
     * Конечное значение диапазона
     */
    private static int finalValue;

    public static void main(String[] args) {
        dataPreparation();
        deletingCompositeNumbers();
        printingSimpleNumbers();
    }

    /**
     * Метод, выполняющий подготовку данных:
     * ввод границ диапазона для работы
     * заполнение массива числами, причем сразу исключая четные (кроме числа 2)
     */
    private static void dataPreparation() {
        Scanner scanner = new Scanner(System.in);
        System.out.println(ENTER_INITIAL_VALUE);
        initialValue = scanner.nextInt();
        while (initialValue < 1) {
            System.out.println(CHANGE_INITIAL_VALUE);
            initialValue = scanner.nextInt();
        }
        System.out.println(ENTER_FINAL_VALUE);
        finalValue = scanner.nextInt();
        while (finalValue <= initialValue) {
            System.out.println(CHANGE_FINAL_VALUE);
            finalValue = scanner.nextInt();
        }
        arrayList.add(2);
        for (int i = 3; i <= finalValue; i += 2) {
            arrayList.add(i);
        }
    }

    /**
     * Метод, удаляющий из списка чисел все составные
     */
    private static void deletingCompositeNumbers() {
        for (int j = 1; j < arrayList.size(); j++) {
            for (int i = j + 1; i < arrayList.size(); i++) {
                if (arrayList.get(i) % arrayList.get(j) == 0) {
                    arrayList.remove(i);
                    i--;
                }
            }
        }
    }

    /**
     * Метод, позволяющий вывести на экран все простые числа в указанном диапазоне
     */
    private static void printingSimpleNumbers() {
        System.out.printf(RANGE_LIMITS, initialValue, finalValue);
        while ((arrayList.indexOf(initialValue) == -1) && (initialValue <= finalValue)) {
            initialValue++;
        }
        if ((initialValue > finalValue)) {
            System.out.println(NO_NUMBERS);
        } else {
            System.out.println(PRINT_NUMBERS);
            for (int i = arrayList.indexOf(initialValue); i < arrayList.size(); i++) {
                System.out.print(arrayList.get(i) + " ");
            }
            System.out.println();
        }
    }
}

/*
    Первоначальный вариант кода
        System.out.print("2 3 5 7 ");
        for (int i = 9; i < 100; i += 2) {
            if ((i % 3 != 0) && (i % 5 != 0) && (i % 7 != 0)) {
                System.out.print(i + " ");
            }
        }
    Результат работы:
    2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97
*/