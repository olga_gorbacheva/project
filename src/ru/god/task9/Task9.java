package ru.god.task9;

import java.util.Scanner;

/**
 * Класс, позволяющий определить, является ли число палиндромом
 *
 * @author Горбачева, 16ИТ18к
 */
public class Task9 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String ENTER_NUMBER = "Введите число, а мы скажем вам, является ли оно палиндромом";
        String IS_PALINDROME = "Это число является палиндромом";
        String IS_NOT_PALINDROME = "Это число не является палиндромом";
        System.out.println(ENTER_NUMBER);
        double number = scanner.nextDouble();
        if (isPalindrome(number)) {
            System.out.println(IS_PALINDROME);
        } else {
            System.out.println(IS_NOT_PALINDROME);
        }
    }

    /**
     * Метод, определяющий, является ли число палиндромом
     *
     * @param number - исходное число
     * @return true, если число является палиндромом, false в любом другом случае
     */
    private static boolean isPalindrome(double number) {
        boolean isPalindrome = false;
        String palindrome = String.valueOf(number);
        char[] symbols = palindrome.toCharArray();
        int indexOfLeftSymbol = 0;
        int indexOfRightSymbol = (symbols.length - 1);
        while (indexOfLeftSymbol < indexOfRightSymbol) {
            if (symbols[indexOfLeftSymbol] != symbols[indexOfRightSymbol]) {
                return isPalindrome;
            } else {
                indexOfLeftSymbol++;
                indexOfRightSymbol--;
                isPalindrome = true;
            }
        }
        return isPalindrome;
    }
}
