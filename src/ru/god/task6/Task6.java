package ru.god.task6;

import java.util.Scanner;

/**
 * Класс, позволяющий получить таблицу умножения для определенного числа
 *
 * @author Горбачева, 16ИТ18к
 */
public class Task6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите число, для которого хотите увидеть таблицу умножения от 1 до 10");
        int number = scanner.nextInt();
        print(number);
    }

    /**
     * Метод, выводящий на экран таблицу умножения
     *
     * @param number - число, для которого рассчитывается таблица
     */
    private static void print(int number) {
        for (int i = 1; i <= 10; i++) {
            System.out.println(number + " * " + i + " = " + (number * i));
        }
    }
}
