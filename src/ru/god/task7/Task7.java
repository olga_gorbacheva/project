package ru.god.task7;

import java.util.Scanner;

/**
 * Класс, позволяющий определить, является ли число вещественным или целым
 *
 * @author Горбачева, 16ИТ18к
 */
public class Task7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите любое число, а мы вам скажем, целое оно или вещественное");
        double number = scanner.nextDouble();
        if (number % 1 == 0) {
            System.out.println("Вы ввели целое число");
        } else {
            System.out.println("Вы ввели вещественное число");
        }
    }
}
