package ru.god.task14;

/**
 * Класс, реализующий транспанирование квадратной матрицы
 *
 * @author Горбачева, 16ИТ18к
 */
public class Task14 {
    public static void main(String[] args) {
        int[][] matrix = {{2, 5, 6}, {5, 3, 9}, {4, 7, 10}};
        print(matrix);
        int[][] newMatrix = new int[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                newMatrix[i][j] = matrix[j][i];
            }
        }
        print(newMatrix);
    }

    /**
     * Метод, позволяющий вывести на экран двумерный массив
     *
     * @param matrix - двумерный массив
     */
    private static void print(int[][] matrix) {
        for (int[] element : matrix) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(element[j] + "\t");
            }
            System.out.println();
        }
        System.out.println();
    }
}
