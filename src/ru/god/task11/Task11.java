package ru.god.task11;

import java.util.Scanner;

/**
 * Класс для вычисления количества часов, минут и секунд в определенном количестве суток
 *
 * @author Горбачева, 16ИТ18к
 */
public class Task11 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите количество суток");
        int days = scanner.nextInt();
        int hours = days * 24;
        int minutes = hours * 60;
        int seconds = minutes * 60;
        System.out.println(days + " суток = " + hours + " часов = " + minutes + " минут = " + seconds + " секунд");
    }
}
