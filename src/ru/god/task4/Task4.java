package ru.god.task4;

import java.util.Scanner;

/**
 * Класс для расчета расстояния до места удара молнии
 *
 * @author Горбачева, 16ИТ18к
 */
public class Task4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double SPEED_OF_SOUND = 1234.8;
        System.out.println("Введите время в секундах между вспышкой и звуком грома");
        double time = scanner.nextDouble();
        System.out.printf("Расстояние до места удара молнии равно %.2f км", (SPEED_OF_SOUND / 3600 * time));
    }
}
