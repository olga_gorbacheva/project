package ru.god.task12;

import java.util.ArrayList;

/**
 * Класс, позволяющий трансформировать двумерный массив построчно в одномерный
 *
 * @author Горбачева, 16ИТ18к
 */
public class Task12 {
    public static void main(String[] args) {
        int[][] matrix = {{2, 5, 6}, {5, 3, 9}, {4, 7, 10}, {12, 1, 8}};
        ArrayList<Integer> arrayList = new ArrayList<>();
        for (int[] array : matrix) {
            for (int element : array) {
                arrayList.add(element);
            }
        }
        for (int number : arrayList) {
            System.out.print(number + " ");
        }
    }
}
