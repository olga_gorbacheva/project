package ru.god.task10;

import java.util.Scanner;

/**
 * Класс, позволяющий определить, является ли строка палиндромом
 *
 * @author Горбачева, 16ИТ18к
 */
public class Task10 {
    public static void main(String[] args) {
        String ENTER_STRING = "Введите выражение, а мы скажем вам, является ли оно палиндромом";
        String IS_PALINDROME = "Это выражение является палиндромом";
        String IS_NOT_PALINDROME = "Это выражение не является палиндромом";
        System.out.println(ENTER_STRING);
        if (isPalindrome(correctString())) {
            System.out.println(IS_PALINDROME);
        } else {
            System.out.println(IS_NOT_PALINDROME);
        }
    }

    /**
     * Метод, позволяющий обработать строку согласно параметрам,
     * т. е. убрать любые символы, кроме букв, и предвидеть возможный ввод пустой строки
     *
     * @return строка, соответствующая параметрам
     */
    private static String correctString() {
        Scanner scanner = new Scanner(System.in);
        String STRING_IS_EMPTY = "Строка пустая, попробуйте еще раз";
        String string = scanner.nextLine();
        while (string.isEmpty()) {
            System.out.println(STRING_IS_EMPTY);
            string = scanner.nextLine();
        }
        return string.toLowerCase().replaceAll("[^a-zа-я0-9]", "");
    }

    /**
     * Метод, определяющий, является ли выражение палиндромом
     *
     * @param string - исходная строка
     * @return true, если строка является палиндромом, false в любом другом случае
     */
    private static boolean isPalindrome(String string) {
        boolean isPalindrome = false;
        char[] symbols = string.toCharArray();
        int indexOfLeftSymbol = 0;
        int indexOfRightSymbol = (symbols.length - 1);
        while (indexOfLeftSymbol < indexOfRightSymbol) {
            if (symbols[indexOfLeftSymbol] != symbols[indexOfRightSymbol]) {
                return isPalindrome;
            } else {
                indexOfLeftSymbol++;
                indexOfRightSymbol--;
                isPalindrome = true;
            }
        }
        return isPalindrome;
    }
}
