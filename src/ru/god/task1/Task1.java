package ru.god.task1;

import java.util.Scanner;

/**
 * Класс, позволяющий обработать строку до точки и посчитать количество пробелов в ней
 *
 * @author Горбачева, 16ИТ18к
 */
public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите строку");
        String string = scanner.nextLine();
        int index = string.indexOf(".");
        String newString = string.substring(0, index);
        System.out.println(newString);
        System.out.println("В строке \"" + string + "\" " + numberOfSpaces(string) + " пробелов");
    }

    /**
     * Метод, позволяющий выполнить подсчет количества пробелов в строке
     *
     * @param string - исходная строка
     * @return количество пробелов в строке
     */
    private static int numberOfSpaces(String string) {
        int numberOfSpaces = 0;
        char[] array = string.toCharArray();
        for (char symbol : array) {
            if (symbol == ' ') {
                numberOfSpaces++;
            }
        }
        return numberOfSpaces;
    }
}
